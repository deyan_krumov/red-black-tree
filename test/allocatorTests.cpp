#include "catch2/catch_all.hpp"
#include "bsTree/Allocator/Allocator.h"

using namespace bst;

struct node
{
    int key;
    std::unique_ptr<node> left{nullptr};
    std::unique_ptr<node> right{nullptr};

    node(const int& _data, std::unique_ptr<node>&& _left = nullptr, std::unique_ptr<node>&& _right = nullptr)
    : key(_data), left(_left ? _left.release() : nullptr), right(_right ? _right.release() : nullptr)
    {
       
    }
};

TEST_CASE("Debug Allocator")
{
    SECTION("Allocates normally, starts throwing after specified num of allocations")
    {
        debugAllocator<node, int, 5> alloc;
        REQUIRE_NOTHROW(alloc.allocate(1));
        REQUIRE_NOTHROW(alloc.allocate(2));
        REQUIRE_NOTHROW(alloc.allocate(3));
        REQUIRE_NOTHROW(alloc.allocate(4));
        REQUIRE_NOTHROW(alloc.allocate(5));
        REQUIRE_THROWS(alloc.allocate(6));
        REQUIRE_THROWS(alloc.allocate(7));
    }

    SECTION("Allocates normally")
    {
        debugAllocator<node, int, 5> alloc;
        std::unique_ptr<node> ptr = alloc.allocate(5);
        REQUIRE(ptr->key == 5);
        REQUIRE(ptr->left == nullptr);
        REQUIRE(ptr->right == nullptr);
    }

    SECTION("Allocates normally with left child being non null")
    {
        debugAllocator<node, int, 5> alloc;
        std::unique_ptr<node> ptr = alloc.allocate(5);

        std::unique_ptr<node> ptr2 = alloc.allocate(10, std::move(ptr));
        REQUIRE(ptr2->key == 10);
        REQUIRE(ptr2->right == nullptr);
        REQUIRE(ptr2->left.get() != ptr.get()); // releases the ownership of ptr
        REQUIRE(ptr.get() == nullptr);

        //Left child is not changed
        REQUIRE(ptr2->left->key == 5);
        REQUIRE(ptr2->left->left == nullptr);
        REQUIRE(ptr2->left->right == nullptr);
    }
}