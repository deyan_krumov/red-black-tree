#include "catch2/catch_all.hpp"
#include "rbTree/rbTree.h"
#include "rotator.h"
#include <iostream>

using namespace rbt;

void print(const std::vector<int>& vec)
{
    for(auto i : vec)
    {
        std::cout << i << " ";
    }
}

TEST_CASE("Left rotate")
{
    rbTree<int> tr;
    tr.testInsert(20);
    tr.testInsert(10);
    tr.testInsert(30);
    tr.testInsert(9);
    tr.testInsert(15);
    tr.testInsert(25);

    auto size = tr.get_size();

    SECTION("Rotation at root")
    {
        rotator<rbTree<int>, rbNode<int>>::leftRotate(tr, (rbNode<int>*)tr.getNode(20));
        std::vector<int> v = {30, 20, 10, 25, 9, 15};
        REQUIRE(tr.bfsPrint() == v);
        REQUIRE(tr.get_size() == size);
    }

    SECTION("Rotation at left child")
    {
        rotator<rbTree<int>, rbNode<int>>::leftRotate(tr, (rbNode<int>*)tr.getNode(10));
        std::vector<int> v = {20, 15, 30, 10, 25, 9};
        REQUIRE(tr.bfsPrint() == v);
        REQUIRE(tr.get_size() == size);
    }

    SECTION("Rotation at right child")
    {
        tr.testInsert(35);
        rotator<rbTree<int>, rbNode<int>>::leftRotate(tr, (rbNode<int>*)tr.getNode(30));
        std::vector<int> v = {20, 10, 35, 9, 15, 30, 25};
        REQUIRE(tr.bfsPrint() == v);
        REQUIRE(tr.get_size() == size+1);
    }
    
}

TEST_CASE("Right rotate")
{
    rbTree<int> tr;
    tr.testInsert(20);
    tr.testInsert(10);
    tr.testInsert(30);
    tr.testInsert(9);
    tr.testInsert(15);
    tr.testInsert(25);

    auto size = tr.get_size();

    SECTION("Rotation at root")
    {
        rotator<rbTree<int>, rbNode<int>>::rightRotate(tr, (rbNode<int>*)tr.getNode(20));
        std::vector<int> v = {10, 9, 20, 15, 30, 25};
        REQUIRE(tr.bfsPrint() == v);
        REQUIRE(tr.get_size() == size);
    }

    SECTION("Rotation at left child")
    {
        rotator<rbTree<int>, rbNode<int>>::rightRotate(tr, (rbNode<int>*)tr.getNode(10));
        std::vector<int> v = {20, 9, 30, 10, 25, 15};
        REQUIRE(tr.bfsPrint() == v);
        REQUIRE(tr.get_size() == size);
    }

    SECTION("Rotation at right child")
    {
        tr.testInsert(35);
        rotator<rbTree<int>, rbNode<int>>::rightRotate(tr, (rbNode<int>*)tr.getNode(30));
        std::vector<int> v = {20, 10, 25, 9, 15, 30, 35};
        REQUIRE(tr.bfsPrint() == v);
        REQUIRE(tr.get_size() == size+1);
    }
    
}