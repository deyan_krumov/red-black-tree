#include "catch2/catch_all.hpp"
#include "bsTree/bsTree.h"
#include <iostream>

using namespace bst;

TEST_CASE("bsTree: Creating a bsTree")
{
    bsTree<int> tr;
    
    SECTION("Size should be 0")
    {
        REQUIRE(tr.get_size() == 0);
    }
}

TEST_CASE("bsTree: Inserting elements")
{
    SECTION("Has enough memory")
    {
        bsTree<int> tr;

        tr.insert(1);
        REQUIRE(tr.get_size() == 1);
        tr.insert(2);
        REQUIRE(tr.get_size() == 2);
        tr.insert(3);
        REQUIRE(tr.get_size() == 3);

        std::vector<int> t = {1,2,3};
        REQUIRE(tr.bfsPrint() == t);
    }

    SECTION("Doesn`t have enough memory")
    {
        bsTree<int, debugAllocator<TreeNode<int>, int, 3>> tr; //debug allocator that only allows 3 allocations
        REQUIRE_NOTHROW(tr.insert(1));
        REQUIRE_NOTHROW(tr.insert(2));
        REQUIRE_NOTHROW(tr.insert(3));
        REQUIRE(tr.get_size() == 3);
        
        REQUIRE_THROWS(tr.insert(4));
        //Check that the tree is unchanged
        REQUIRE(tr.get_size() == 3);
        std::vector<int> t = {1,2,3};
        REQUIRE(tr.bfsPrint() == t);

        REQUIRE_THROWS(tr.insert(5));
        REQUIRE(tr.get_size() == 3);
        REQUIRE(tr.bfsPrint() == t);
    }
}

TEST_CASE("bsTree: bfsPrint test")
{
    bsTree<int> tr;
    SECTION("Trying to print empty tree should throw")
    {
        REQUIRE_THROWS(tr.bfsPrint());
    }

    SECTION("Inline tree")
    {
        tr.insert(1);
        tr.insert(2);
        tr.insert(3);

        std::vector<int> v = {1,2,3};
        REQUIRE(tr.bfsPrint() == v);
    }

    SECTION("Balanced tree")
    {
        tr.insert(20);
        tr.insert(10);
        tr.insert(15);
        tr.insert(30);
        tr.insert(25);
        tr.insert(9);

        std::vector<int> v = {20, 10, 30, 9, 15, 25};
        REQUIRE(tr.bfsPrint() == v);
    }
}

TEST_CASE("bsTree: Copying tree")
{
    bsTree<int> tr;
    tr.insert(20);
    tr.insert(10);
    tr.insert(15);
    tr.insert(30);
    tr.insert(25);
    tr.insert(9);

    SECTION("Copy constructor")
    {
        bsTree<int> cpy(tr);
        REQUIRE(cpy.get_size() == tr.get_size());
        REQUIRE(cpy.bfsPrint() == tr.bfsPrint());
        
        SECTION("Check if it an actual coppy")
        {
            // int* ptr = &cpy.get(10);
            // std::cout << *ptr << '\n';
            cpy.get(10) = 100;
            // std::cout << *ptr << '\n';

            REQUIRE(tr.contains(10));
            REQUIRE_FALSE(tr.contains(100));

            // REQUIRE_NOTHROW(cpy.get(100));
            // REQUIRE(cpy.contains(100));
            REQUIRE_FALSE(cpy.contains(10));
        }
    }

    SECTION("Copy operator")
    {
        bsTree<int> cpy;
        cpy.insert(1);
        cpy.insert(2);
        cpy.insert(3);
        REQUIRE(cpy.get_size() == 3);

        cpy = tr;
        REQUIRE(cpy.get_size() == tr.get_size());
        REQUIRE(cpy.bfsPrint() == tr.bfsPrint());

        // SECTION("Check if it an actual coppy")
        // {
        //     cpy.get(10) = 100;
        //     REQUIRE(cpy.contains(100));
        //     REQUIRE_FALSE(cpy.contains(10));

        //     REQUIRE(tr.contains(10));
        //     REQUIRE_FALSE(tr.contains(100));
        // }
    }
}

TEST_CASE("bsTree: Move semantics")
{
    bsTree<int> tr;
    tr.insert(20);
    tr.insert(10);
    tr.insert(15);
    tr.insert(30);
    tr.insert(25);
    tr.insert(9);

    SECTION("Move constructor")
    {
        bsTree<int> cpy(std::move(tr));
        REQUIRE(cpy.get_size() == 6);
        REQUIRE(tr.get_size() == 0);

        std::vector<int> v = {20, 10, 30, 9, 15, 25};
        REQUIRE(cpy.bfsPrint() == v);
        REQUIRE_THROWS(tr.bfsPrint());
    }

    SECTION("Move operator")
    {
        bsTree<int> cpy;
        cpy.insert(1);
        cpy.insert(2);
        cpy.insert(3);

        cpy = bsTree<int>{};
        REQUIRE(cpy.get_size() == 0);
        REQUIRE_THROWS(cpy.bfsPrint());
    }
}

TEST_CASE("bsTree: Searching for elements")
{
    bsTree<std::string> tr;
    tr.insert("Hello");
    tr.insert("Wide");
    tr.insert("World");

    SECTION("Testing getting elements non-const version")
    {
        REQUIRE_NOTHROW(tr.get("Hello"));
        REQUIRE_NOTHROW(tr.get("Wide"));
        REQUIRE_NOTHROW(tr.get("World"));

        REQUIRE_THROWS(tr.get("Shkembe chorba"));
        REQUIRE_THROWS(tr.get("Mesanica"));
        REQUIRE_THROWS(tr.get("Tarator"));
    }

    SECTION("Testing getting elements const version")
    {
        const bsTree<std::string> constant(tr);
        
        REQUIRE_NOTHROW(constant.get("Hello"));
        REQUIRE_NOTHROW(constant.get("Wide"));
        REQUIRE_NOTHROW(constant.get("World"));

        REQUIRE_THROWS(constant.get("Shkembe chorba"));
        REQUIRE_THROWS(constant.get("Mesanica"));
        REQUIRE_THROWS(constant.get("Tarator"));
    }

    SECTION("Contains method")
    {
        REQUIRE(tr.contains("Hello"));
        REQUIRE(tr.contains("Wide"));
        REQUIRE(tr.contains("World"));

        REQUIRE_FALSE(tr.contains("Shrek 3"));
        REQUIRE_FALSE(tr.contains("Bee the movie"));
    }
}

TEST_CASE("bsTree: get_height")
{
    SECTION("linear tree")
    {
        bsTree<int> tr;
        tr.insert(1);
        tr.insert(2);
        tr.insert(3);
        tr.insert(4);

        REQUIRE(tr.get_height() == 4);
    }

    SECTION("Normal tree")
    {
        bsTree<int> tr;
        tr.insert(10);
        tr.insert(15);
        tr.insert(5);
        tr.insert(2);
        tr.insert(8);
        tr.insert(4);
        tr.insert(7);
        tr.insert(6);
        tr.insert(9);

        REQUIRE(tr.get_height() == 5);
    }

    SECTION("Empty tree")
    {
        bsTree<int> tr;
        REQUIRE(tr.get_height() == 0);
    }
}

TEST_CASE("bsTree: clear")
{
    bsTree<int> tr;
    tr.insert(1);
    tr.insert(2);
    tr.insert(3);
    tr.insert(4);

    tr.clear();
    REQUIRE(tr.get_size() == 0);
    REQUIRE(tr.get_height() == 0);
    REQUIRE(tr.empty());
}

TEST_CASE("bsTree: swap function")
{
    bsTree<int> tr;
    tr.insert(1);
    tr.insert(2);
    tr.insert(3);
    tr.insert(4);

    auto v = tr.bfsPrint();
    auto size = tr.get_size();

    bsTree<int> tr1;
    tr1.insert(5);
    tr1.insert(6);
    tr1.insert(7);
    tr1.insert(8);
    
    auto v1 = tr1.bfsPrint();
    auto size1 = tr1.get_size();

    swap(tr, tr1);
    REQUIRE(tr.bfsPrint() == v1);
    REQUIRE(tr1.bfsPrint() == v);
    REQUIRE(tr.get_size() == size1);
    REQUIRE(tr1.get_size() == size);
}

// TEST_CASE("bsTree: Erasing elemensts")
// {
//     SECTION("Erasing empty should yield err")
//     {
//         bsTree<int> tr;
//         REQUIRE_THROWS(tr.erase(10));
//     }

//     SECTION("Erasing root")
//     {
//         bsTree<int> tr;
//         tr.insert(10);
//         tr.erase(10);
//         REQUIRE(tr.get_size() == 0);
//         REQUIRE_THROWS(tr.bfsPrint());
//         REQUIRE_FALSE(tr.contains(10));
//     }

//     SECTION("Erasing node with only left child")
//     {
//         bsTree<int> tr;
//         tr.insert(3);
//         tr.insert(2);
//         tr.insert(50);
//         tr.insert(10);
//         tr.insert(6);
//         tr.insert(15);


//         tr.erase(50);
//         REQUIRE_FALSE(tr.contains(50));
//         REQUIRE(tr.get_size() == 5);
//         std::vector<int> v = {3, 2, 10, 6, 15};
//         REQUIRE(tr.bfsPrint() == v);
//     }

//     SECTION("Erasing node with only right child")
//     {
//         bsTree<int> tr;
//         tr.insert(4);
//         tr.insert(1);
//         tr.insert(5);
//         tr.insert(2);
//         tr.insert(3);

//         tr.erase(1);
//         REQUIRE_FALSE(tr.contains(1));
//         REQUIRE(tr.get_size() == 4);
//         std::vector<int> v = {4, 2, 5, 3};
//         REQUIRE(tr.bfsPrint() == v);
//     }

//     SECTION("Erase node with 2 children root")
//     {
//         bsTree<int> tr;
//         tr.insert(10);
//         tr.insert(5);
//         tr.insert(15);

//         tr.erase(10);
//         REQUIRE_FALSE(tr.contains(10));
//         REQUIRE(tr.get_size() == 2);
//         std::vector<int> v = {15, 5};
//         REQUIRE(tr.bfsPrint() == v);
//     }

//     SECTION("Erase node with 2 children furthermost left child of root->right has no children")
//     {
//         bsTree<int> tr;
//         tr.insert(10);
//         tr.insert(15);
//         tr.insert(5);
//         tr.insert(2);
//         tr.insert(8);
//         tr.insert(4);
//         tr.insert(7);
//         tr.insert(6);
//         tr.insert(9);
//         std::vector<int> ex = {10, 5, 15, 2, 8, 4, 7, 9, 6};
//         REQUIRE(tr.bfsPrint() == ex);

//         tr.erase(5);
//         REQUIRE_FALSE(tr.contains(5));
//         REQUIRE(tr.get_size() == 8);
//         std::vector<int> v = {10, 6, 15, 2, 8, 4, 7, 9};
//         REQUIRE(tr.bfsPrint() == v);
//     }
// }

