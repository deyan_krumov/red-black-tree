#include "catch2/catch_all.hpp"
#include "rbTree/rbtree.h"
#include <iostream>

void print(const std::vector<int>& vec)
{
    for(auto i : vec)
    {
        std::cout << i << " ";
    }
}

using namespace rbt;

TEST_CASE("Test rbNode methods")
{
    SECTION("Relations between nodes: grandparent, child, uncle, sibling")
    {
        SECTION("Root node")
        {
            rbNode<int> n(5);
            REQUIRE(n.grandparent() == nullptr);
            REQUIRE(n.sibling() == nullptr);
            REQUIRE(n.uncle() == nullptr);
        }

        rbTree<int> tr;
        tr.testInsert(20);
        tr.testInsert(10);
        tr.testInsert(30);
        tr.testInsert(9);
        tr.testInsert(15);
        tr.testInsert(25);

        SECTION("Level two node, left child")
        {
            rbNode<int>* node = (rbNode<int>*)tr.getNode(10);
            rbNode<int>* sibling = (rbNode<int>*)tr.getNode(30);
            REQUIRE(node->grandparent() == nullptr);
            REQUIRE(node->sibling() == sibling);
            REQUIRE(node->uncle() == nullptr);
        }

        SECTION("Level two node, RIGHT child")
        {
            rbNode<int>* node = (rbNode<int>*)tr.getNode(30);
            rbNode<int>* sibling = (rbNode<int>*)tr.getNode(10);
            REQUIRE(node->grandparent() == nullptr);
            REQUIRE(node->sibling() == sibling);
            REQUIRE(node->uncle() == nullptr);
        }

        SECTION("Deeper level node, right child")
        {
            rbNode<int>* node = (rbNode<int>*)tr.getNode(15);
            rbNode<int>* sibling = (rbNode<int>*)tr.getNode(9);
            rbNode<int>* grandparent = (rbNode<int>*)tr.getNode(20);
            rbNode<int>* uncle = (rbNode<int>*)tr.getNode(30);
            REQUIRE(node->grandparent() == grandparent);
            REQUIRE(node->sibling() == sibling);
            REQUIRE(node->uncle() == uncle);
        }

        SECTION("Deeper level node, left child")
        {
            rbNode<int>* node = (rbNode<int>*)tr.getNode(25);
            rbNode<int>* grandparent = (rbNode<int>*)tr.getNode(20);
            rbNode<int>* uncle = (rbNode<int>*)tr.getNode(10);
            REQUIRE(node->grandparent() == grandparent);
            REQUIRE(node->sibling() == nullptr);
            REQUIRE(node->uncle() == uncle);
        }
    }

    SECTION("getLeft & getRight")
    {
        SECTION("Root")
        {
            rbNode<int> n(4);
            REQUIRE(n.getLeft() == nullptr);
            REQUIRE(n.getRight() == nullptr);
        }

        SECTION("Not root")
        {
            rbTree<int> tr;
            tr.testInsert(20);
            tr.testInsert(30);
            tr.testInsert(10);
            tr.testInsert(40);
            tr.testInsert(25);

            auto n = (rbNode<int>*)tr.getNode(30);
            auto left = (rbNode<int>*)tr.getNode(25);
            auto right = (rbNode<int>*)tr.getNode(40);
            REQUIRE(n->getLeft() == left);
            REQUIRE(n->getRight() == right);
        }
    }
}

TEST_CASE("rbTree: Creating a rbTree")
{
    rbTree<int> tr;
    REQUIRE(tr.get_size() == 0);
    REQUIRE(tr.get_height() == 0);
    REQUIRE_THROWS(tr.bfsPrint());
}

TEST_CASE("Inserting elements") 
{
    rbTree<int> tr;

    SECTION("Tree is empty")
    {
        tr.insert(1);
        REQUIRE(tr.get_size() == 1);
        std::vector<int> v = {1};
        REQUIRE(tr.bfsPrint() == v);
        rbNode<int>* root = (rbNode<int>*)tr.getNode(1);
        REQUIRE(root->color == Color::BLACK);
    }

    SECTION("Parent is black")
    {
        tr.insert(1);
        tr.insert(2);
        REQUIRE(tr.get_size() == 2);
        std::vector<int> v = {1, 2};
        REQUIRE(tr.bfsPrint() == v);
        rbNode<int>* n = (rbNode<int>*)tr.getNode(2);
        REQUIRE(n->color == Color::RED);
    }

    SECTION("Parent is red")
    {
        SECTION("Uncle is red too but grandparent is root")
        {
            tr.insert(1);
            tr.insert(2);
            tr.insert(0);
            tr.insert(3);
            REQUIRE(tr.get_size() == 4);
            std::vector<int> v = {1, 0, 2, 3};
            REQUIRE(tr.bfsPrint() == v);
            rbNode<int>* n = (rbNode<int>*)tr.getNode(3);
            REQUIRE(n->color == Color::RED);
            REQUIRE(n->parent->color == Color::BLACK);
            REQUIRE(n->uncle()->color == Color::BLACK);
            REQUIRE(n->grandparent()->color == Color::BLACK);
        }

        SECTION("Uncle is red too and grandparent is not root")
        {
            tr.insert(61);
            tr.insert(52);
            tr.insert(85);
            tr.insert(76);
            tr.insert(93);
            tr.insert(100);
            REQUIRE(tr.get_size() == 6);
            std::vector<int> v = {61, 52, 85, 76, 93, 100};
            REQUIRE(tr.bfsPrint() == v);

            rbNode<int>* n = (rbNode<int>*)tr.getNode(100);
            REQUIRE(n->color == Color::RED);
            REQUIRE(n->parent->color == Color::BLACK);
            REQUIRE(n->uncle()->color == Color::BLACK);
            REQUIRE(n->grandparent()->color == Color::RED);
        }

        SECTION("Uncle is black or null")
        {
           SECTION("Parent is right child of Grandparent and newNode is right child of parent")
           {
                tr.insert(61);
                tr.insert(52);
                tr.insert(85);
                tr.insert(93);
                tr.insert(100);
                std::vector<int> v = {61, 52, 93, 85, 100};
                REQUIRE(tr.bfsPrint() == v);

                rbNode<int>* n = (rbNode<int>*)tr.getNode(100);
                REQUIRE(n->color == Color::RED);
                auto sibling = (rbNode<int>*)tr.getNode(85);
                REQUIRE(n->sibling() == sibling);
                REQUIRE(sibling->color == Color::RED);
                REQUIRE(n->parent->color == Color::BLACK);
           }

           SECTION("Parent is right child of Grandparent and newNode is left child of parent")
           {
                tr.insert(61);
                tr.insert(52);
                tr.insert(85);
                tr.insert(93);
                tr.insert(87);
                std::vector<int> v = {61, 52, 87, 85, 93};
                REQUIRE(tr.bfsPrint() == v);

                rbNode<int>* n = (rbNode<int>*)tr.getNode(93);
                REQUIRE(n->color == Color::RED);
                auto sibling = (rbNode<int>*)tr.getNode(85);
                REQUIRE(n->sibling() == sibling);
                REQUIRE(sibling->color == Color::RED);
                REQUIRE(n->parent->color == Color::BLACK);
           }

           SECTION("Parent is left child of Grandparent and newNode is left child of parent")
           {
                tr.insert(61);
                tr.insert(52);
                tr.insert(85);
                tr.insert(50);
                tr.insert(40);
                std::vector<int> v = {61, 50, 85, 40, 52};
                REQUIRE(tr.bfsPrint() == v);

                rbNode<int>* n = (rbNode<int>*)tr.getNode(40);
                REQUIRE(n->color == Color::RED);
                auto sibling = (rbNode<int>*)tr.getNode(52);
                REQUIRE(n->sibling() == sibling);
                REQUIRE(sibling->color == Color::RED);
                REQUIRE(n->parent->color == Color::BLACK);
           }

           SECTION("Parent is left child of Grandparent and newNode is right child of parent")
           {
                tr.insert(61);
                tr.insert(52);
                tr.insert(85);
                tr.insert(50);
                tr.insert(51);
                std::vector<int> v = {61, 51, 85, 50, 52};
                REQUIRE(tr.bfsPrint() == v);

                rbNode<int>* n = (rbNode<int>*)tr.getNode(51);
                REQUIRE(n->color == Color::BLACK);
                auto left = (rbNode<int>*)tr.getNode(50);
                auto right = (rbNode<int>*)tr.getNode(52);
                REQUIRE(n->getLeft() == left);
                REQUIRE(n->getRight() == right);
                REQUIRE(left->color == Color::RED);
                REQUIRE(n->parent->color == Color::BLACK);
                REQUIRE(left->sibling()->color == Color::RED);
           }
        }
    }
}

TEST_CASE("rbTree: Copying tree")
{
    rbTree<int> tr;
    tr.insert(20);
    tr.insert(10);
    tr.insert(15);
    tr.insert(30);
    tr.insert(25);
    tr.insert(9);

    SECTION("Copy constructor")
    {
        rbTree<int> cpy(tr);
        REQUIRE(cpy.get_size() == tr.get_size());
        REQUIRE(cpy.bfsPrint() == tr.bfsPrint());
        
        SECTION("Check if it an actual coppy")
        {
            // int* ptr = &cpy.get(10);
            // std::cout << *ptr << '\n';
            cpy.get(10) = 100;
            // std::cout << *ptr << '\n';

            REQUIRE(tr.contains(10));
            REQUIRE_FALSE(tr.contains(100));

            // REQUIRE_NOTHROW(cpy.get(100));
            // REQUIRE(cpy.contains(100));
            REQUIRE_FALSE(cpy.contains(10));
        }
    }

    SECTION("Copy operator")
    {
        rbTree<int> cpy;
        cpy.insert(1);
        cpy.insert(2);
        cpy.insert(3);
        REQUIRE(cpy.get_size() == 3);

        cpy = tr;
        REQUIRE(cpy.get_size() == tr.get_size());
        REQUIRE(cpy.bfsPrint() == tr.bfsPrint());

        // SECTION("Check if it an actual coppy")
        // {
        //     cpy.get(10) = 100;
        //     REQUIRE(cpy.contains(100));
        //     REQUIRE_FALSE(cpy.contains(10));

        //     REQUIRE(tr.contains(10));
        //     REQUIRE_FALSE(tr.contains(100));
        // }
    }
}

TEST_CASE("rbTree: Move semantics")
{
    rbTree<int> tr;
    tr.insert(20);
    tr.insert(10);
    tr.insert(15);
    tr.insert(30);
    tr.insert(25);
    tr.insert(9);

    auto v = tr.bfsPrint();

    SECTION("Move constructor")
    {
        rbTree<int> cpy(std::move(tr));
        REQUIRE(cpy.get_size() == 6);
        REQUIRE(tr.get_size() == 0);

        REQUIRE(cpy.bfsPrint() == v);
        REQUIRE_THROWS(tr.bfsPrint());
    }

    SECTION("Move operator")
    {
        rbTree<int> cpy;
        cpy.insert(1);
        cpy.insert(2);
        cpy.insert(3);

        cpy = rbTree<int>{};
        REQUIRE(cpy.get_size() == 0);
        REQUIRE_THROWS(cpy.bfsPrint());
    }
}

TEST_CASE("rbTree: Searching for elements")
{
    rbTree<std::string> tr;
    tr.insert("Hello");
    tr.insert("Wide");
    tr.insert("World");

    SECTION("Testing getting elements non-const version")
    {
        REQUIRE_NOTHROW(tr.get("Hello"));
        REQUIRE_NOTHROW(tr.get("Wide"));
        REQUIRE_NOTHROW(tr.get("World"));

        REQUIRE_THROWS(tr.get("Shkembe chorba"));
        REQUIRE_THROWS(tr.get("Mesanica"));
        REQUIRE_THROWS(tr.get("Tarator"));
    }

    SECTION("Testing getting elements const version")
    {
        const rbTree<std::string> constant(tr);
        
        REQUIRE_NOTHROW(constant.get("Hello"));
        REQUIRE_NOTHROW(constant.get("Wide"));
        REQUIRE_NOTHROW(constant.get("World"));

        REQUIRE_THROWS(constant.get("Shkembe chorba"));
        REQUIRE_THROWS(constant.get("Mesanica"));
        REQUIRE_THROWS(constant.get("Tarator"));
    }

    SECTION("Contains method")
    {
        REQUIRE(tr.contains("Hello"));
        REQUIRE(tr.contains("Wide"));
        REQUIRE(tr.contains("World"));

        REQUIRE_FALSE(tr.contains("Shrek 3"));
        REQUIRE_FALSE(tr.contains("Bee the movie"));
    }
}

TEST_CASE("rbTree: clear")
{
    rbTree<int> tr;
    tr.insert(1);
    tr.insert(2);
    tr.insert(3);
    tr.insert(4);

    tr.clear();
    REQUIRE(tr.get_size() == 0);
    REQUIRE(tr.get_height() == 0);
    REQUIRE(tr.empty());
}

TEST_CASE("rbTree: swap function")
{
    rbTree<int> tr;
    tr.insert(1);
    tr.insert(2);
    tr.insert(3);
    tr.insert(4);

    auto v = tr.bfsPrint();
    auto size = tr.get_size();

    rbTree<int> tr1;
    tr1.insert(5);
    tr1.insert(6);
    tr1.insert(7);
    tr1.insert(8);
    
    auto v1 = tr1.bfsPrint();
    auto size1 = tr1.get_size();

    swap(tr, tr1);
    REQUIRE(tr.bfsPrint() == v1);
    REQUIRE(tr1.bfsPrint() == v);
    REQUIRE(tr.get_size() == size1);
    REQUIRE(tr1.get_size() == size);
}