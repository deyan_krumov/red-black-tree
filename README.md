# Red Black Tree
A simple header-only C++ library.
It includes: 
- red-black tree and binary search tree implementations. ✅
- (**99.7% free of memory leaks**) as it uses unique_ptr for memory management. ✅
- Custom fine-tuned logging class powered by [spdlog](https://github.com/gabime/spdlogcom). Configured to write your error messages, exceptions, etc to both console and files so you can share them with friends! 🎉
- Some out-of-the-box tests powered by [Catch2](https://github.com/catchorg/Catch2.git) and configured with CTest. But you don`t have to worry about them. The code works perfectly, trust me. 💯👌 

> Note: the `erase` method for both trees doesn`t work as intended, might patch it later.

## Getting started

### Cloning the repository 📄
As this repo contains submodules so it is recommended to clone it using:
```sh
git clone --recurse-submodules git@gitlab.com:deyan_krumov/red-black-tree.git
```
### Default building settings 🛠
- ENABLE_TESTING: ON ("Specifies whether to build the tests") 
- BUILD_DOCUMENTATION: OFF ("Specifies whether to build the project documentation")
- ENABLE_SPDLOG: ON ("Specifies whether to build spdlog library")

### Building and Running 👷‍♂️
Create build folder and initialize CMake
```sh
mkdir build
cd ./build
cmake ../
```
To build using the default settings use:
```sh
cmake --build .
```
> Note: All ``Binary``, and ``Log`` files will be generated inside `build/bin` folder
> Note: ``Documentation`` files will be generated inside `build/docs` folder
## Dependencies 🌎
### Logging: 
- spdlog: https://github.com/gabime/spdlog
### Tests:
- Catch2: https://github.com/catchorg/Catch2.git
### Documentation:
>Note: Should install separately on your local machine if you want documentation
- doxygen: https://www.doxygen.nl/download.html
- graphviz(uml graphs inside documentation): http://www.graphviz.org/download/
## License

MIT

**Free Software, Hell Yeah!**