#pragma once
#include "../treeInterface.h"
#include "./Allocator/Allocator.h"
#include "../logger/logger.h"
#include <vector>
#include <queue>

namespace bst
{
    static dl::Logger logger("bsTree");

    /**
     * @brief internal node of a bsTree
     * 
     * @tparam dataType 
     */
    #define BsNode TreeNode<dataType>

    /**
     * @brief Binary search tree implementation.
     * @link https://en.wikipedia.org/wiki/Binary_search_tree @endlink
     * 
     * @tparam dataType data to be kept inside the tree
     * @tparam allocatorType class its allocate() method is used to create new bsNodes 
     */
    template <typename dataType, typename allocatorType = defAllocator<BsNode, dataType>> 
    class bsTree : public Tree<dataType>
    {
    protected:
        std::unique_ptr<BsNode> root{nullptr};
        std::size_t size = 0;

    private:
        allocatorType allocator;

    public:
        bsTree() = default;
        virtual ~bsTree() = default;
        
        /**
         * @brief A copy constructor. It copies the size, allocator and root of the other tree.
         * 
         * @param other tree to be copied
         * @exception May throw std::bad_alloc or any exception thrown by the constructor of T. If an exception is thrown, this function has no effect.
         */
        bsTree(const bsTree& other) : size(other.size), allocator(other.allocator), root(copy(other.root)) {logger->debug("Entered copy ctor");}

        bsTree& operator=(const bsTree& other)
        {
            logger->debug("Entered copy operator");

            if(this != &other)
            {
                bsTree<dataType> cpy(other);
                std::swap(size, cpy.size);
                std::swap(allocator, cpy.allocator);
                std::swap(root, cpy.root);
            }
            return *this;
        }

        bsTree (bsTree&& rhs) : bsTree() {
            logger->debug("Entered move ctor");

            *this = std::move(rhs);  
        }

        bsTree& operator=(bsTree&& rhs)
        {
            logger->debug("Entered move operator=");

            if(this != &rhs)
            {
                std::swap(root, rhs.root);
                std::swap(size, rhs.size);
                std::swap(allocator, rhs.allocator);
            }
            return *this;
        }
        
        /**
         * @brief insert new node
         * 
         * @param value to be inserted
         * @exception May throw std::bad_alloc or any exception thrown by the constructor of T. If an exception is thrown, this function has no effect.
         */
        virtual void insert(const dataType& value) override {insert(root, value); logger->debug("Inserting element {}", value);}
        virtual size_t get_size() const override {return size;}

        /**
         * @brief Recursive function that searches for a key inside the tree.
         * 
         * @param root node to start from
         * @param key value to search for
         * @return const dataType& 
         * @exception invalid_argument if key is not found
         */
        virtual dataType& get(const dataType& key) override
        {
            return *const_cast<dataType*>(&(const_cast<const bsTree*>(this)->get(key)));
        }

        virtual const dataType& get(const dataType& key) const override
        {
            return getNode(key)->data;
        }

        const BsNode* getNode(const dataType& key) const
        {
            logger->debug("Searching for element {}", key);

            try
            {
                return get(root, key).get();
            }
            catch(const std::invalid_argument& e)
            {
                logger->warn("Element {} not found", key);
                throw e;
            }
        }

        BsNode* getNode(const dataType& key)
        {
            return const_cast<BsNode*>(const_cast<const bsTree*>(this)->getNode(key));
        }

        virtual void erase(const dataType& key) 
        {
            // remove(root, key); 
            logger->debug("Erasing element {}", key);
        }

        ///@brief Checking if the tree contains a certain element.
        virtual bool contains( const dataType& key ) const override
        {
            try
            {
                get(key);
            }
            catch(const std::invalid_argument& e)
            {
                return false;
            }
            return true;
        }

        virtual void clear() override {root.reset(); size = 0; logger->debug("Clearing tree");}

        virtual bool empty() const override {return !(size && root);}

        virtual size_t get_height() const override {return height(root);}

        /**
         * @brief function that return a vector filled with the elements of the tree in a breadth first search manner.
         * 
         * @return std::vector<dataType> 
         */
        std::vector<dataType> bfsPrint() const {logger->debug("Entered bfsPrint"); return bfsPrint(root);}

    private:
        void insert(std::unique_ptr<BsNode>& root, const dataType& value)
        {
            if (!root) 
            {
                root = getAllocation(value);
                ++size;
            }
            else 
            {   
                insert(((value < root->data) ? root->left : root->right), value);
            }
        }

        /**
         * @brief wrapper for allocate(), used to log in case of exceptions
         * 
         * @param value 
         * @param left leftsubree
         * @param right rightsubtree
         * @return std::unique_ptr<BsNode> 
         * @exception bad_alloc() or other exception thrown by bsNode constructor
         */
        std::unique_ptr<BsNode> getAllocation( const dataType& value, std::unique_ptr<BsNode>&& left = nullptr, std::unique_ptr<BsNode>&& right = nullptr)
        {
            try
            {
                return allocator.allocate(value, std::move(left), std::move(right));
            }
            catch(const std::exception& e)
            {
                logger->critical("Failed to allocate memory exception: {}", e.what());
                throw e;
            }
        }

        /**
         * @brief A recursive function that copies the tree.
         * 
         * @param root from this node it starts to copy
         * @return BsNode* 
         */
        BsNode* copy(const std::unique_ptr<BsNode>& root)
        {
            if(root)
            {
                BsNode* leftSubTree = copy(root->left); //Recursively copies until it reaches nullptr
                BsNode* rightSubTree = copy(root->right);
                std::unique_ptr<BsNode> node = getAllocation(root->data, std::unique_ptr<BsNode>(leftSubTree), std::unique_ptr<BsNode>(rightSubTree));

                return node.release(); //Transfer the ownership of the data from local to outside scope
            }
            else 
                return root.get(); //nullptr
        }

        /**
         * @brief Recursive function that searches for a key inside the tree.
         * 
         * @param root node to start from
         * @param key value to search for
         * @return const dataType& 
         * @exception invalid_argument if key is not found
         */
        const std::unique_ptr<BsNode>& get(const std::unique_ptr<BsNode>& root, const dataType& key) const
        {
            if(!root) throw std::invalid_argument("Element does not exist inside this container");
            if(root->data == key) return root;
            return key < root->data ? get(root->left, key) : get(root->right, key);
        }

        // void remove(std::unique_ptr<BsNode>& root, const dataType& key)
        // {
        //     if (!root) 
        //     {
        //         logger->error("Can`t erase element from an empty tree");
        //         throw std::invalid_argument("Trying to erase non existant element");
        //     }

        //     if (root->data == key) {
        //         // node* toDel = root;
        //         if (!root->left) {
        //             root.reset(root->right.release());
        //         }
        //         else if (!root->right) {
        //             root.reset(root->left.release());
        //         }
        //         else {
        //             BsNode* rightChild = (root->right.release());
        //             BsNode* mR = extractMin(&rightChild);
        //             mR->left.reset(root->left.release());
        //             mR->right.reset(rightChild);
        //             root.reset(mR);
        //         }
        //         --size;
        //         // delete toDel;
        //     }
        //     else {
        //         return remove(key < root->data ? root->left : root->right, key);
        //     }
        // }

        // BsNode* extractMin(BsNode** root)
        // {
        //     if ((*root)->left) {
        //         BsNode* leftChild = (*root)->left.get();
        //         return extractMin(&leftChild);
        //     }

        //     BsNode* n = *root;
        //     if((*root)->right)
        //         (*root) = (*root)->right.release();
        //     else
        //         (*root) = nullptr;
        //     return n;
        // }

        size_t height(const std::unique_ptr<BsNode>& root) const
        {
            if (!root) return 0;
            return 1 + std::max(height(root->left), height(root->right));
        }

        /// @brief  function that prints the tree in a breadth first search manner.
        std::vector<dataType> bfsPrint(const std::unique_ptr<BsNode>& root) const
        {
            std::vector<dataType> vec;
            // Base Case
            if (!root)
            {
                logger->error("Invalid argument given to bfsPrint, root cannot be null");
                throw std::invalid_argument("Root is null");
            }
        
            // Create an empty queue for level order traversal
            std::queue<BsNode*> q;
        
            // Enqueue Root and initialize height
            q.push(root.get());
        
            while (q.empty() == false) {
                // Print front of queue and remove it from queue
                BsNode* node = q.front();
                vec.push_back(node->data);
                q.pop();
        
                /* Enqueue left child */
                if (node->left != nullptr)
                    q.push(node->left.get());
        
                /*Enqueue right child */
                if (node->right != nullptr)
                    q.push(node->right.get());
            }
            return vec;
        }
    };

    /// A template function that swaps the contents of two bsTree objects.
    template <typename dataType, typename allocatorType = defAllocator<BsNode, dataType>>
    void swap(bsTree<dataType, allocatorType>& a, bsTree<dataType, allocatorType>& b)
    {
        a = std::move(b);
    }
} //namespace bst





