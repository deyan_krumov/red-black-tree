#pragma once
#include <memory>

template<typename dataType>
struct TreeNode
{
    dataType data;
    std::unique_ptr<TreeNode> left{nullptr};
    std::unique_ptr<TreeNode> right{nullptr};

    /// Constructor, when it gets unique_ptr it transfers the ownership of the data managed from the arg to the unique_ptr inside the TreeNode
    TreeNode(const dataType& _data, std::unique_ptr<TreeNode>&& _left = nullptr, std::unique_ptr<TreeNode>&& _right = nullptr)
    : data(_data), left(_left ? _left.release() : nullptr), right(_right ? _right.release() : nullptr) {}
};

template <typename dataType>
class Tree
{
public:
    virtual bool contains( const dataType& key ) const = 0;
    virtual const dataType& get( const dataType& key) const = 0;
    virtual dataType& get( const dataType& key) = 0;
    virtual void insert( const dataType& value ) = 0;
    virtual void erase( const dataType& key ) = 0;
    virtual void clear() = 0;
    virtual bool empty() const = 0;
    virtual size_t get_size() const = 0;
    virtual size_t get_height() const = 0;
};