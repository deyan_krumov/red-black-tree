#pragma once
#include <unordered_set>
#include <memory>

namespace rbt
{
    enum class Color : uint8_t 
    {
        RED,
        BLACK
    };

    /// @brief Base class for the allocators. T - node type, keyType - key data type inside the node
    template <typename T, typename keyType>
    class Allocator
    {
    public:
        virtual std::unique_ptr<T> allocate(const keyType& _data, T* _parent = nullptr, const Color& _color = Color::RED,
                std::unique_ptr<T>&& _left = nullptr, std::unique_ptr<T>&& _right = nullptr) = 0;
        virtual std::size_t getSizeOfType() {return sizeof(T);}
    };

    /**
     * @brief Default allocator, allocates memory for node and wraps it in unique_ptr
     * 
     * @tparam T node type
     * @tparam keyType key data type inside the node
     */
    template <typename T, typename keyType>
    class defAllocator : public Allocator<T, keyType>
    {
    public:
        virtual std::unique_ptr<T> allocate(const keyType& _data, T* _parent = nullptr, const Color& _color = Color::RED,
                std::unique_ptr<T>&& _left = nullptr, std::unique_ptr<T>&& _right = nullptr) override 
        {
            return std::make_unique<T>(_data, _parent, _color, std::move(_left), std::move(_right));
        }
    };

    /**
     * @brief Custom allocator that is used to test the code. It is a simple allocator that throws an exception when allocating if the number of allocations exceeds a certain number.
     * 
     * @tparam T node type
     * @tparam keyType key data type inside the node
     */
    template <typename T, typename keyType, std::size_t allowedAllocations>
    class debugAllocator : public defAllocator<T, keyType>
    {
        size_t allocations{0};
        size_t numAllowedAllocs;
    public:
        debugAllocator() : numAllowedAllocs(allowedAllocations) {}

        virtual std::unique_ptr<T> allocate(const keyType& _data, T* _parent = nullptr, const Color& _color = Color::RED,
                std::unique_ptr<T>&& _left = nullptr, std::unique_ptr<T>&& _right = nullptr) override 
        {
            allocations++;
            if(allocations > numAllowedAllocs)
                throw std::bad_alloc();
            
            return defAllocator<T, keyType>::allocate(_data, _parent, _color, std::move(_left), std::move(_right));
        }
    };
} //Namespace rbt
