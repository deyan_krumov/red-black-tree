#pragma once
#include "../bsTree/bsTree.h"
#include "./Allocator/Allocator.h"
#include "../logger/logger.h"
#include "../rotator.h"
#include <iostream>

namespace rbt
{
    static dl::Logger logger("rbTree");

    /**
     * @brief internal node of a rbTree
     *
     * @tparam dataType
     */
    template <typename dataType>
    struct rbNode : public TreeNode<dataType>
    {
        Color color;
        rbNode* parent{}; // Normal ptr because if there is a parent node it is allocated and managed by unique_ptr

        rbNode(const dataType& _data, rbNode* _parent = nullptr, const Color& _color = Color::RED,
        std::unique_ptr<TreeNode<dataType>>&& _left = nullptr, std::unique_ptr<TreeNode<dataType>>&& _right = nullptr)
        : TreeNode<dataType>::TreeNode(_data, std::move(_left), std::move(_right)),
        color(_color),
        parent(_parent) {}

        rbNode* getLeft()
        {
            return (rbNode*)this->left.get();
        }

        rbNode* getRight()
        {
            return (rbNode*)this->right.get();
        }

        rbNode* grandparent() {return parent ? parent->parent : nullptr;}
        rbNode* sibling()
        {
            if(parent)
            {
                if(parent->right.get() == this) return (rbNode<dataType>*)parent->left.get();
                else return (rbNode<dataType>*)parent->right.get();
            }
            return nullptr;
        }
        rbNode* uncle() {return parent ? parent->sibling() : nullptr;}
    };

    #define RbNode rbNode<dataType>

    /**
     * @brief Red Black tree implementation.
     * @link https://en.wikipedia.org/wiki/Red%E2%80%93black_tree @endlink
     *
     * @tparam dataType data to be kept inside the tree
     * @tparam allocatorType class its allocate() method is used to create new rbNodes
     */
    template <typename dataType, typename allocatorType = defAllocator<RbNode, dataType>>
    class rbTree : public bst::bsTree<dataType>
    {
        allocatorType allocator;

    public:
        rbTree() = default;
        virtual ~rbTree() = default;

        friend class rotator<rbTree<dataType, allocatorType>, RbNode>;

        /**
         * @brief A copy constructor. It copies the size, allocator and root of the other tree.
         *
         * @param other tree to be copied
         * @exception May throw std::bad_alloc or any exception thrown by the constructor of T. If an exception is thrown, this function has no effect.
         */
        rbTree(const rbTree& other)
        {
            this->size = other.size;
            this->allocator = other.allocator;
            this->root.reset(copy(other.root));
            logger->debug("Entered copy ctor");
        }

        rbTree& operator=(const rbTree& other)
        {
            logger->debug("Entered copy operator");

            if(this != &other)
            {
                rbTree<dataType> cpy(other);
                std::swap(this->size, cpy.size);
                std::swap(this->allocator, cpy.allocator);
                std::swap(this->root, cpy.root);
            }
            return *this;
        }

        rbTree (rbTree&& rhs) : rbTree() {
            logger->debug("Entered move ctor");

            *this = std::move(rhs);
        }

        rbTree& operator=(rbTree&& rhs)
        {
            logger->debug("Entered move operator=");

            if(this != &rhs)
            {
                std::swap(this->root, rhs.root);
                std::swap(this->size, rhs.size);
                std::swap(this->allocator, rhs.allocator);
            }
            return *this;
        }

        /**
         * @brief A recursive function that prints the tree in a nice format.
         *
         * @param out out stream to be used
         */
        void print(std::ostream& out = std::cout) const {printBT(this->root, out);}

        /**
         * @brief Inserts new element and if needed rebalances and recolors the nodes inside the tree
         *
         * @param value to be inserted
         */
        virtual void insert(const dataType& value) override {
            logger->debug("Inserting element {}", value);

            RbNode* newNode = this->bsInsert(this->root, value); //Normal bs tree insert

            if(newNode->parent == nullptr) // Case new node is root
            {
                newNode->color = Color::BLACK;
                return;
            }
            else if(newNode->grandparent() == nullptr) return;

            fixTree(newNode); //Rebalance the tree
        }

        /**
         * @brief Public test insert that uses normal binary search tree insert, used to test rotations
         * @warning REMOVE FROM PRODUCTION BUILD
         *
         * @param value
         */
        void testInsert(const dataType& value){bsInsert(this->root, value);}

        virtual void erase( const dataType& key ) override
        {
            deleteNodeHelper(key);
        }

    private:

        /// Ordinary binary search insert
        RbNode* bsInsert(std::unique_ptr<BsNode>& root, const dataType& value)
        {
            RbNode* parent{};
            std::unique_ptr<BsNode>* newNode = &root;
            while(*newNode)
            {
                parent = (RbNode*)newNode->get();
                (value < (*newNode)->data) ? newNode = &(*newNode)->left : newNode = &(*newNode)->right;
            }

            *newNode = getAllocation(value, parent); //By default it colors RED
            this->size++;
            return (RbNode*)(*newNode).get();
        }

        /**
         * @brief The above code is fixing the tree after insertion
         *
         * @param node node that was inserted most recently
         */
        void fixTree(RbNode* node)
        {
            RbNode* uncle{};
            while (node->parent->color == Color::RED)
            {
                uncle = node->uncle();
                if(uncle && uncle->color == Color::RED) // case parent is red and uncle too
                {
                    uncle->color = Color::BLACK;
                    node->parent->color = Color::BLACK;
                    node->grandparent()->color = Color::RED;
                    node = node->grandparent();
                }
                else if(node->parent == node->grandparent()->getRight()) // If parent is right child
                {
                    if(node == node->parent->getLeft()) // parent is right child of grandparent and node is left child of parent
                    {
                        node = node->parent;
                        rotator<rbTree<dataType, allocatorType>, RbNode>::rightRotate(*this, node);
                    }

                    // Case node is right child of parent
                    node->parent->color = Color::BLACK;
                    node->grandparent()->color = Color::RED;
                    rotator<rbTree<dataType, allocatorType>, RbNode>::leftRotate(*this, node->grandparent());
                }
                else //Parent is left child
                {
                    if (node == node->parent->getRight()) // parent is left child of grandparent and node is right child of parent
                    {
                        node = node->parent;
                        rotator<rbTree<dataType, allocatorType>, RbNode>::leftRotate(*this, node);
                    }

                    //Case node is left child of parent
                    node->parent->color = Color::BLACK;
                    node->grandparent()->color = Color::RED;
                    rotator<rbTree<dataType, allocatorType>, RbNode>::rightRotate(*this, node->grandparent());
                }
                if(node == (RbNode*)this->root.get())
                {
                    break;
                }
            }
            ((RbNode*)this->root.get())->color = Color::BLACK;
        }

        /**
         * @brief A recursive function that copies the tree.
         *
         * @param root from this node it starts to copy
         * @return RbNode*
         */
        BsNode* copy(const std::unique_ptr<BsNode>& root)
        {
            if(root)
            {
                RbNode* n = (RbNode*)root.get();
                RbNode* leftSubTree = (RbNode*)copy(n->left); //Recursively copies until it reaches nullptr
                RbNode* rightSubTree = (RbNode*)copy(n->right);
                std::unique_ptr<BsNode> node = getAllocation(n->data, n->parent, n->color, std::unique_ptr<RbNode>(leftSubTree), std::unique_ptr<RbNode>(rightSubTree));

                return node.release(); //Transfer the ownership of the data from local to outside scope
            }
            else
                return root.get(); //nullptr
        }

        /**
         * @brief wrapper for allocate(), used to log in case of exceptions
         *
         * @param value
         * @param parent parent of node
         * @param color color of node
         * @param left leftsubree
         * @param right rightsubtree
         * @return std::unique_ptr<RbNode>
         * @exception bad_alloc() or other exception thrown by bsNode constructor
         */
        std::unique_ptr<RbNode> getAllocation(const dataType& value, RbNode* parent = nullptr, const Color& color = Color::RED,
                                std::unique_ptr<RbNode>&& left = nullptr, std::unique_ptr<RbNode>&& right = nullptr)
        {
            try
            {
                return this->allocator.allocate(value, parent, color, std::move(left), std::move(right));
            }
            catch(const std::exception& e)
            {
                logger->critical("Failed to allocate memory exception: {}", e.what());
                throw e;
            }
        }

        /**
         * @brief A recursive function that prints the tree in a nice format.
         *
         * @param prefix Symbol to be displayed infront of first node
         * @param node starting point
         * @param isLeft
         * @param out out stream
         */
        void printBT(const std::string& prefix, const std::unique_ptr<BsNode>& node, const bool& isLeft, std::ostream& out) const
        {
            if(node)
            {
                out << prefix;

                out << (isLeft ? "|---" : "'---" );

                // print the value of the node
                RbNode* n = (RbNode*)node.get();
                out << n->data << " " << printColor(n->color) << " ";
                if(n->parent)
                    out << n->parent->data;
                out << '\n';
                // enter the next tree level - left and right branch
                printBT( prefix + (isLeft ? "|   " : "    "), node->left, true, out);
                printBT( prefix + (isLeft ? "|   " : "    "), node->right, false, out);
            }
        }

        void printBT(const std::unique_ptr<BsNode>& node, std::ostream& out = std::cout) const
        {
            printBT("", node, false, out);
        }

        const std::string printColor(const Color& color) const
        {
            switch (color)
            {
            case Color::RED:
                return "RED";
                break;
            case Color::BLACK:
                return "BLACK";
                break;
            default:
                return "INVALID";
                break;
            }
        }

        void deleteNodeHelper(const dataType& key)
        {
            RbNode* x;
            RbNode* y;
            RbNode* node = (RbNode*)this->getNode(key); //If element doesn`t exist it throws

            y = node;
            Color yOriginalColor = y->color;

            bsDelete(node, y, x, yOriginalColor);
            if(yOriginalColor == Color::BLACK)
                fixDelete(x);
            
            this->size--;
        }

        void bsDelete(RbNode*& node, RbNode*& y, RbNode*& x, Color& yOriginalColor)
        {
            if (node->left == nullptr) //Has only right child
            {
                x = node->getRight();
                transplant(node, node->right, true);
            } else if (node->right == nullptr) //Has only left child
            {
                x = node->getLeft();
                transplant(node, node->left, true);
            } else {
                y = minimumNode(node->getRight());
                yOriginalColor = y->color;
                x = y->getRight();
                if (x && y->parent == node) {
                    x->parent = y;
                } else if(y->right) {
                    transplant(y, y->right, false); //False flag - means that we don`t delete y, because we will use it below. It is assigned to node
                    y->right.reset(node->right.get());
                    y->getRight()->parent = y;
                }

                y->color = node->color;
                y->left.reset(node->left.get());
                y->getLeft()->parent = y;
                if(y == y->parent->getLeft())
                {
                    transplant(node, y->parent->left, true);
                }
                else
                {
                    transplant(node, y->parent->right, true);
                }
            }
        }

        bool isLeftChild(const RbNode* n) const
        {
            if(n == n->parent->getLeft()) return true;
            else return false;
        }

        /**
         * @brief replaces Old node with New, by connecting Old`s parent to New
         * 
         * @param Old node to be replaced
         * @param New node to replace with
         * @param deleteOld flag whether or not to delete the old node
         */
        void transplant(RbNode*& Old, std::unique_ptr<BsNode>& New, bool deleteOld) //Forgive me uncle Bob for I have used a bool flag :(
        {
            castToRbNode(New)->parent = Old->parent;
            if (Old->parent == nullptr)
            {
                if(!deleteOld) this->root.release(); //Releases but doesn`t delete
                this->root.reset(New.release());
            }
            else if (Old == Old->parent->getLeft()) // is Left child
            {
                if(!deleteOld) Old->parent->left.release();
                Old->parent->left.reset(New.release());
            } else // is Right child
            {
                if(!deleteOld) Old->parent->right.release();
                Old->parent->right.reset(New.release());
            }
	    }

        RbNode* castToRbNode(const std::unique_ptr<BsNode>& ptr) const
        {
            return (RbNode*)ptr.get();
        }

        /// find the node with the minimum key
        RbNode* minimumNode(RbNode* node) {
            while (node->left) {
                node = node->getLeft();
            }
            return node;
        }

        /// fix the rb tree modified by the delete operation
        void fixDelete(RbNode* x) {
            RbNode* s;
            while (x != (RbNode*)this->root.get() && x->color == Color::BLACK) 
            {
                bool isXLeftChild = (x == x->parent->getLeft());
                s = x->sibling();
                if (s->color == Color::RED) {
                    // new node`s sibling is red
                    s->color = Color::BLACK;
                    x->parent->color = Color::RED;

                    if(isXLeftChild)
                        rotator<rbTree<dataType, allocatorType>, RbNode>::leftRotate(*this, x->parent);
                    else 
                        rotator<rbTree<dataType, allocatorType>, RbNode>::rightRotate(*this, x->parent);

                    s = x->sibling();
                }

                if (s->getLeft()->color == Color::BLACK && s->getRight()->color == Color::BLACK) {
                    // new node`s sibling is black and both of sibling`s children are black
                    s->color = Color::RED;
                    x = x->parent;
                } else {
                    if (s->getRight()->color == Color::BLACK) {
                        // new node sibling is black, sibling`s left child is red and its right child iis black
                        s->getLeft()->color = Color::BLACK;
                        s->color = Color::RED;
                        rotator<rbTree<dataType, allocatorType>, RbNode>::rightRotate(*this, s);
                        s = x->sibling();
                    } 
                    else if(s->getLeft()->color == Color::BLACK) {
                        // same case as above but mirrored
                        s->getRight()->color = Color::BLACK;
                        s->color = Color::RED;
                        rotator<rbTree<dataType, allocatorType>, RbNode>::leftRotate(*this, s);
                        s = x->sibling();
                    }

                    // new node`s sibling is black and its right child is red
                    s->color = x->parent->color;
                    x->parent->color = Color::BLACK;
                    if(isXLeftChild)
                    {
                        s->getRight()->color = Color::BLACK;
                        rotator<rbTree<dataType, allocatorType>, RbNode>::leftRotate(*this, x->parent);
                    }
                    else
                    {
                        s->getLeft()->color = Color::BLACK;
                        rotator<rbTree<dataType, allocatorType>, RbNode>::rightRotate(*this, x->parent);
                    }
                    x = (RbNode*)this->root.get();
                }
            }
            x->color = Color::BLACK;
        }
    };


    /// A template function that swaps the contents of two rbTree objects.
    template <typename dataType, typename allocatorType = defAllocator<RbNode, dataType>>
    void swap(rbTree<dataType, allocatorType>& a, rbTree<dataType, allocatorType>& b)
    {
        a = std::move(b);
    }
} //Namespace rbt



