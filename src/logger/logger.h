#pragma once

#include "spdlog/spdlog.h"
#include "spdlog/sinks/basic_file_sink.h" // support for basic file logging
#include "spdlog/sinks/stdout_color_sinks.h"


namespace dl
{
    const static std::string DEFAULT_LOG_OUTPUT_PATH = "logs/log.txt";

    /**
     * @brief Class designed to consume the log library
     */
    class Logger
    {
        spdlog::logger logger;

    public:

        /**
         * The above function is a constructor for the Logger class. It takes a string as a parameter and
         * creates a logger object with the name of the string. The logger object is then initialized with
         * a file sink and a console sink
         * 
         * @param name The name of the logger.
        */
        Logger(const std::string& name);

        Logger(const Logger& other) = delete;
        Logger& operator=(const Logger& other) = delete;
        ~Logger() = default;

        spdlog::logger* operator->();

    private:

        /**
         * `initFileSink` creates a file sink and sets the log level to `warn`
         * 
         * @param logDirectory The directory where the log file will be created.
         * 
         * @return A pointer to a shared_ptr of a basic_file_sink_mt.
         */
        std::shared_ptr<spdlog::sinks::basic_file_sink_mt> initFileSink(const std::string& logDirectory = DEFAULT_LOG_OUTPUT_PATH);

        /**
         * `initConsoleSink` creates a console sink and sets the log level to debug
         * 
         * @return A pointer to a console_sink object.
         */
        std::shared_ptr<spdlog::sinks::stdout_color_sink_mt> initConsoleSink();
    };
} // end namespace dl