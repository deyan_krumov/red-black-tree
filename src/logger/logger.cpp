#include "logger.h"

namespace dl
{
    Logger::Logger(const std::string &name) : logger(name, {initFileSink("logs/" + name + ".txt"), initConsoleSink()})
    {
        logger.set_level(spdlog::level::trace);
    }

    spdlog::logger* Logger::operator->()
    {
        return &logger;
    }

    std::shared_ptr<spdlog::sinks::basic_file_sink_mt> Logger::initFileSink(const std::string &logDirectory)
    {
        auto file_sink = std::make_shared<spdlog::sinks::basic_file_sink_mt>(logDirectory, true);
        file_sink->set_level(spdlog::level::debug);
        return file_sink;
    }

    std::shared_ptr<spdlog::sinks::stdout_color_sink_mt> Logger::initConsoleSink()
    {
        auto console_sink = std::make_shared<spdlog::sinks::stdout_color_sink_mt>();
        console_sink->set_level(spdlog::level::warn);
        return console_sink;
    }

} // end namespace dl