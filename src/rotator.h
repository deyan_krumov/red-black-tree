#pragma once

template <typename treeType, typename nodeType>
class rotator
{
public:

/*
            |                                                |
            |                                                |
            |                                                |
            |                                                |
         +--+--+                                          +--+-+
  +------+  x  +-----+      ---------------->        +----+ y  +-----+
  |      +-----+     |                               |    +----+     |
  |                  |                            +--+--+         +--+--+
+-+-+             +--+--+                     +---+  x  +---+     |  C  |
| A |        +----+  y  +---+                 |   +-----+   |     +-----+
+---+        |    +-----+   |                 |             |
          +--+-+         +--+-+           +---+-+         +-+--+
          | B  |         |  C |           |  A  |         |  B |
          +----+         +----+           +-----+         +----+
 */
    static void leftRotate(treeType& tree, nodeType* x)
    {
        nodeType* y = x->getRight();
		x->right.release(); //Releasing before resrting so the data won`t be lost
		x->right.reset(y->left.release());
		if (y->left) {
			y->getLeft()->parent = x;
		}
		y->parent = x->parent;
		if (x->parent == nullptr) {
			tree.root.release();
			tree.root.reset(y);
		} else if (x == x->parent->getLeft()) {
			x->parent->left.release();
			x->parent->left.reset(y);
		} else {
			x->parent->right.release();
			x->parent->right.reset(y);
		}
		y->left.reset(x);
		x->parent = y;
    }

/*
                   |                                                |
                   |                                                |
                   |                                                |
                   |                                                |
                +--+-+                                           +--+--+
           +----+ y  +-----+                              +------+  x  +-----+
           |    +----+     |             ---------->      |      +-----+     |
        +--+--+         +--+--+                           |                  |
    +---+  x  +---+     |  C  |                         +-+-+             +--+--+
    |   +-----+   |     +-----+                         | A |        +----+  y  +---+
    |             |                                     +---+        |    +-----+   |
+---+-+         +-+--+                                            +--+-+         +--+-+
|  A  |         |  B |                                            | B  |         |  C |
+-----+         +----+                                            +----+         +----+
 */
    static void rightRotate(treeType& tree, nodeType* x) 
	{
		nodeType* y = x->getLeft();
		x->left.release();
		x->left.reset(y->right.release());
		if (y->right) {
			y->getRight()->parent = x;
		}
		y->parent = x->parent;
		if (x->parent == nullptr) {
			tree.root.release();
			tree.root.reset(y);
		} else if (x == x->parent->getRight()) {
			x->parent->right.release();
			x->parent->right.reset(y);
		} else {
			x->parent->left.release();
			x->parent->left.reset(y);
		}
		y->right.reset(x);
		x->parent = y;
	}
};